package com.kind.perm.core.system.dao.impl;


import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.system.dao.OrganizationDao;
import com.kind.perm.core.system.domain.OrganizationDO;

/**
 * 
 * 机构信息数据访问实现类. <br/>
 * 
 * @date:2017-03-02 09:24:35 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class OrganizationDaoImpl extends BaseDaoMyBatisImpl<OrganizationDO, Serializable> implements OrganizationDao {

	@Override
	public List<OrganizationDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(OrganizationDO entity) {
		//KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
	}

	@Override
	public OrganizationDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public List<OrganizationDO> queryList(OrganizationDO entity) {
		return super.query(NAMESPACE + "queryList", entity);
	}

}
