package com.kind.common.task;

public  interface Task<T> {
	 T execute(TaskContext context);
}
